Example React application.

- [ ]	Pull the set of (fake) currency rates from /api/currency and store them in the Redux application store.
- [ ]	Pull the set of stored values (from/to currency types and amounts) from /api/stored-values.
- [ ]	Wire up the user interface to edit the values fetched from the server (currency table is read-only, the “stored values” would be edited by the user).
- [ ]	When the user finishes editing, write the stored values back to /api/stored-values using a POST operation.
