import axios from "axios";
import { ValuesData } from '../Models';

export const getCurrencyRates = async () => {
  try {
    const { data } = await axios.get("https://localhost:5001/api/currency");
    return data;
  } catch (e) {
    return console.error(e);
  }
};

export const getStoredValues = async () => {
  try {
    const data = await axios.get("https://localhost:5001/api/stored-values");
    return data.data;
  } catch (e) {
    return console.error(e);
  }
};

export const writeStoredValues = async (values: ValuesData) => {
  try {
    const { data } = await axios.post(
      "https://localhost:5001/api/stored-values",
      values
    );
    return data;
  } catch (e) {
    return e;
  }
};
