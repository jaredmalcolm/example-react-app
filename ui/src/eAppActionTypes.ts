export enum eAppActionTypes {
    FETCH_EXCHANGE_RATES,
    STORE_EXCHANGE_RATES,
    FETCH_VALUES,
    SAVE_VALUES_TO_SERVER,
    STORE_VALUES,
}

export default eAppActionTypes;