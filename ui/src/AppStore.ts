import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

import { initialState, reducer } from "./reducers/AppActions";

declare global {
  interface Window {
    [key: string]: any;
  }
}

export const appStore = createStore(
  reducer,
  initialState,
  applyMiddleware(thunk),
);
