import { CurrencyData, ValuesData } from "../Models";
import { AnyAction } from "redux";

import Actions from "../eAppActionTypes";
import { getCurrencyRates, getStoredValues, writeStoredValues } from "../lib/currencyServices";

export interface AppStoreState {
  currency: CurrencyData;
  values: ValuesData;
}

export const initialState: AppStoreState = {
  currency: {
    name: "CURRENCY",
    exchangeRates: {
      USD: {
        GBP: 0.0,
        EUR: 0.0
      },
      GBP: {
        USD: 0.0,
        EUR: 0.0
      },
      EUR: {
        USD: 0.0,
        GBP: 0.0
      }
    }
  },
  values: {
    fromCurrency: "USD",
    fromAmount: 0.0,
    toCurrency: "GBP",
    toAmount: 0.0
  }
};

export const fetchCurrencyValues = () => {
  return (dispatch: any) => {
    getCurrencyRates().then(currencies =>
      dispatch(loadCurrencyValues(currencies))
    );
  };
};
export const loadCurrencyValues = (values: CurrencyData) => ({
  type: Actions.FETCH_EXCHANGE_RATES,
  payload: values
});

export const fetchStoredValues = () => {
  return (dispatch: any) => {
    getStoredValues().then(values => dispatch(loadStoredValues(values)));
  };
};
export const loadStoredValues = (values: ValuesData) => ({
  type: Actions.FETCH_VALUES,
  payload: values
});

export const updateStoredValues = (values: ValuesData) => async (
  dispatch: any
) => {
  if (await writeStoredValues(values)) {
    dispatch(fetchStoredValues());
  }
};

export function reducer(state: AppStoreState, action: AnyAction) {
  switch (action.type) {
    case Actions.FETCH_EXCHANGE_RATES:
      return {
        ...state,
        currency: action.payload
      };
    case Actions.FETCH_VALUES:
      return {
        ...state,
        values: action.payload
      };
    case Actions.SAVE_VALUES_TO_SERVER:
      return state;
    case Actions.STORE_EXCHANGE_RATES:
      return state;
    case Actions.STORE_VALUES:
      return state;
    default:
      return state;
  }
}
