import * as React from 'react';
import {Provider} from "react-redux";
import './App.css';

import ExchangeRates from "./ExchangeRates";
import SelectedValuesForm from "./SelectedValuesForm";

import {appStore} from "./AppStore";

export class App extends React.Component {
  public render() {
    return (
      <Provider store={appStore}>
        <div className="App">
          <header>
            <h1>Example</h1>
          </header>
          <div>
            <ExchangeRates/>
            <SelectedValuesForm/>
          </div>
        </div>
      </Provider>
    );
  }
}
