import * as React from "react";
import { useEffect, useRef } from "react";
import { connect } from "react-redux";

import { fetchStoredValues, AppStoreState, updateStoredValues } from "./reducers/AppActions";

const mapStateToProps = (state: AppStoreState) => ({
  values: state.values
});

export const SelectedValuesForm = (props: any) => {
  const { fromCurrency, fromAmount, toCurrency, toAmount } = props.values;

  const fromCurrencyRef = useRef(null);
  const fromAmountRef = useRef(null);
  const toCurrencyRef = useRef(null);
  const toAmountRef = useRef(null);

  useEffect(() => {
    props.fetchStoredValues();
  }, [ fromCurrency, fromAmount, toCurrency, toAmount ]);

  const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = {
      fromCurrency: (fromCurrencyRef.current as any).value,
      fromAmount: (fromAmountRef.current as any).value,
      toCurrency: (toCurrencyRef.current as any).value,
      toAmount: (toAmountRef.current as any).value,
    };

    props.updateStoredValues(formData);
  };

  return (
    <div>
      <form onSubmit={handleFormSubmit}>
        <fieldset>
          <legend>Select Values</legend>
          <div>
            <label>
              From Currency:
              <select defaultValue={fromCurrency} ref={fromCurrencyRef}>
                <option>USD</option>
                <option>GBP</option>
                <option>EUR</option>
              </select>
            </label>
          </div>
          <div>
            <label>
              From Amount:
              <input type="text" defaultValue={fromAmount} ref={fromAmountRef} />
            </label>
          </div>
          <div>
            <label>
              To Currency:
              <select defaultValue={toCurrency} ref={toCurrencyRef}>
                <option>USD</option>
                <option>GBP</option>
                <option>EUR</option>
              </select>
            </label>
          </div>
          <div>
            <label>
              To Amount:
              <input type="text" defaultValue={toAmount} ref={toAmountRef} />
            </label>
          </div>
          <div>
            <button type="submit">Save</button>
          </div>
        </fieldset>
      </form>
    </div>
  );
};

export default connect<{}, {}>(
  mapStateToProps,
  { fetchStoredValues, updateStoredValues }
)(SelectedValuesForm);