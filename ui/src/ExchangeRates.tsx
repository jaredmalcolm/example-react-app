import * as React from "react";
import { useEffect } from "react";
import { connect } from "react-redux";

import { fetchCurrencyValues, AppStoreState } from "./reducers/AppActions";

const mapStateToProps = (state: AppStoreState) => ({
  currency: state.currency
});

export const ExchangeRates = (props: any) => {
  const { USD, GBP, EUR } = props.currency.exchangeRates;

  useEffect(() => {
    props.fetchCurrencyValues();
  }, [USD.GBP, USD.EUR, GBP.USD, GBP.EUR, EUR.USD, EUR.GBP]);

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th style={{backgroundColor: '#000000'}} />
            <th>USD</th>
            <th>GBP</th>
            <th>EUR</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>USD</th>
            <td style={{backgroundColor: '#dddddd'}}>&mdash;</td>
            <td>{USD.GBP}</td>
            <td>{USD.EUR}</td>
          </tr>
          <tr>
            <th>GBP</th>
            <td>{GBP.USD}</td>
            <td style={{backgroundColor: '#dddddd'}}>&mdash;</td>
            <td>{GBP.EUR}</td>
          </tr>
          <tr>
            <th>EUR</th>
            <td>{EUR.USD}</td>
            <td>{EUR.GBP}</td>
            <td style={{backgroundColor: '#dddddd'}}>&mdash;</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default connect<{}, {}>(
  mapStateToProps,
  { fetchCurrencyValues }
)(ExchangeRates);
